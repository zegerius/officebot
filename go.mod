module gitlab.com/zegerius/officebot

go 1.14

require (
	github.com/go-redis/redis/v8 v8.6.0
	github.com/robfig/cron/v3 v3.0.0
	github.com/slack-go/slack v0.8.1
)
