package main

import (
	"github.com/go-redis/redis/v8"
	"gitlab.com/zegerius/officebot/bot"
	"gitlab.com/zegerius/officebot/internal/config"
)

func main() {
	cfg := config.NewConfig()
	redisClient := redis.NewClient(&redis.Options{
		Addr:     cfg.RedisURL,
		Password: "",
		DB:       0,
	})
	defer redisClient.Close()

	bot := bot.NewBot(cfg, redisClient)
	bot.Run()
}
