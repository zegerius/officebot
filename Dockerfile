FROM golang:1.16-alpine AS build
RUN apk add --no-cache tzdata
WORKDIR /src
COPY . .
RUN go build -o /out/officebot .

FROM alpine:latest AS bin
RUN apk add --no-cache tzdata
COPY --from=build /out/officebot /

CMD ["./officebot"]
