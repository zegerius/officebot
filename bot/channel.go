package bot

import (
	"fmt"
	"strings"

	"github.com/robfig/cron/v3"
)

type Option struct {
	Name string `json:"name"`
	Max  *int   `json:"max"`
}

type CronSchedule struct {
	Single   string `json:"single"`
	Overview string `json:"overview"`
}

type ChannelConfig struct {
	Timezone     string              `json:"timezone"`
	CronSchedule CronSchedule        `json:"cron_schedule"`
	CronJobIDs   []cron.EntryID      `json:"-"`
	Options      []Option            `json:"options"`
	UserAttrs    map[string][]string `json:"user_attrs"`
}

type Channel string

func (b *Bot) GetChannels() []Channel {
	keys, err := b.Storage.GetKeysByPattern("channel:*")
	if err != nil {
		return nil
	}

	var channels []Channel
	for _, key := range keys {
		channel := strings.TrimPrefix(key, "channel:")
		channels = append(channels, Channel(channel))
	}

	return channels
}

func (b *Bot) GetChannelConfig(channel Channel) *ChannelConfig {
	key := fmt.Sprintf("channel:%s", channel)

	if config, ok := b.Channels[channel]; ok {
		return config
	}

	var channelConfig ChannelConfig
	err := b.Storage.Get(key, &channelConfig)
	if err != nil {
		return nil
	}

	b.Channels[channel] = &channelConfig
	return &channelConfig
}

func (b *Bot) AddChannel(channel Channel, config *ChannelConfig) {
	key := fmt.Sprintf("channel:%s", channel)
	err := b.Storage.Set(key, config)
	if err != nil {
		fmt.Println(err)
	}

	b.Channels[channel] = config
}

func (b *Bot) DeleteChannel(channel Channel) {
	err := b.Storage.Delete(fmt.Sprintf("channel:%s", channel))
	if err != nil {
		fmt.Println(err)
	}

	delete(b.Channels, channel)
}
