package bot

import (
	"fmt"
	"strings"

	"github.com/slack-go/slack"
	"gitlab.com/zegerius/officebot/internal/message"
)

func (b *Bot) SendSingleMsg(channel Channel, config ChannelConfig) func() {
	return func() {
		fmt.Println("Sending single message")
		dateStr, dateKey := message.GetNextWorkDay()

		poll := b.GetPoll(channel, dateKey)

		params := generateDayMessage(poll, &config, dateStr, dateKey)

		_, ts, err := b.SlackClient.PostMessage(string(channel), slack.MsgOptionText("", false), params)
		if err != nil {
			fmt.Println(err)
		}

		// Save message ID
		poll.MessageID = ts
		b.SetPoll(channel, dateKey, poll)
	}
}

func (b *Bot) SendOverviewMsg(channel Channel, config ChannelConfig) func() {
	return func() {
		fmt.Println("Sending overview message")
		workWeek := message.GetNextWorkWeek()

		var polls = make(map[string]*Poll)
		for _, day := range workWeek {
			dateKey := day[1]
			poll := b.GetPoll(channel, dateKey)
			polls[dateKey] = poll
		}

		params := generateWeekMessage(polls, &config, workWeek)

		_, ts, err := b.SlackClient.PostMessage(string(channel), slack.MsgOptionText("", false), params)
		if err != nil {
			fmt.Println("Something went wrong")
		}

		b.ClearPins(channel)
		b.AddPin(channel, ts)

		for _, day := range workWeek {
			dateKey := day[1]
			poll := b.GetPoll(channel, dateKey)
			poll.OverviewMessageID = ts
			b.SetPoll(channel, dateKey, poll)
		}
	}
}

func generateDayMessage(poll *Poll, config *ChannelConfig, dateStr string, dateKey string) slack.MsgOption {
	//header
	headerText := fmt.Sprintf("Where are you working on *%s*?", dateStr)
	headerTextBlock := slack.NewTextBlockObject("mrkdwn", headerText, false, false)
	headerSection := slack.NewSectionBlock(headerTextBlock, nil, nil)

	// Divider
	dividerBlock := slack.NewDividerBlock()

	// Initialize blocks with header and divider
	var blocks []slack.Block
	blocks = append(blocks, headerSection, dividerBlock)

	// Iterate through each option
	for _, option := range config.Options {
		userIDs := poll.Results[option.Name]
		var userNames []string
		for _, userID := range userIDs {
			userName := fmt.Sprintf("<@%s>", userID)
			if emojis, ok := config.UserAttrs[userName]; ok {
				userName += " " + strings.Join(emojis, " ")
			}
			userNames = append(userNames, userName)
		}

		// Count text
		countText := fmt.Sprintf("%d", len(userNames))
		if option.Max != nil && len(userNames) >= *option.Max {
			countText += " :warning:"
		}

		// Option text
		optionText := fmt.Sprintf("*%s*\nGoing (%s): %s", option.Name, countText, strings.Join(userNames, ", "))
		optionTextBlock := slack.NewTextBlockObject("mrkdwn", optionText, false, false)

		// Button text and button
		buttonText := slack.NewTextBlockObject("plain_text", fmt.Sprintf("Choose %s", option.Name), true, false)
		button := slack.NewButtonBlockElement(dateKey, option.Name, buttonText)
		textAccessory := slack.NewAccessory(button)
		optionSection := slack.NewSectionBlock(optionTextBlock, nil, textAccessory)

		// Append the constructed block for this option
		blocks = append(blocks, optionSection)
	}

	// Build Message with blocks created above
	params := slack.MsgOptionBlocks(blocks...)

	return params
}

func generateWeekMessage(polls map[string]*Poll, config *ChannelConfig, workWeek [][]string) slack.MsgOption {
	// Header
	headerTextBlock := slack.NewTextBlockObject("mrkdwn", "*Where are you working next week?*", false, false)
	headerSection := slack.NewSectionBlock(headerTextBlock, nil, nil)

	// Divider
	dividerBlock := slack.NewDividerBlock()

	// Initialize blocks with header and divider
	var blocks []slack.Block
	blocks = append(blocks, headerSection, dividerBlock)

	for _, day := range workWeek {
		dateStr := day[0] // Display date
		dateKey := day[1] // Key used in poll results

		// Create date section
		sectionText := fmt.Sprintf("*%s*", dateStr)
		sectionTextBlock := slack.NewTextBlockObject("mrkdwn", sectionText, false, false)

		// Dropdown element
		selectText := slack.NewTextBlockObject("plain_text", "Select an option", true, false)
		var options []*slack.OptionBlockObject
		for _, option := range config.Options {
			optionText := slack.NewTextBlockObject("plain_text", option.Name, true, false)
			options = append(options, slack.NewOptionBlockObject(option.Name, optionText, nil))
		}
		availableOption := slack.NewOptionsSelectBlockElement("static_select", selectText, dateKey, options...)

		// Check if we have poll data for this day
		poll, ok := polls[dateKey]
		var contextElements []slack.MixedElement
		if ok {
			// Iterate through each option
			for _, option := range config.Options {
				userIDs := poll.Results[option.Name]
				var userNames []string
				for _, userID := range userIDs {
					userName := fmt.Sprintf("<@%s>", userID)
					if emojis, ok := config.UserAttrs[userName]; ok {
						userName += " " + strings.Join(emojis, " ")
					}
					userNames = append(userNames, userName)
				}

				// Count text
				countText := fmt.Sprintf("%d", len(userNames))
				if option.Max != nil && len(userNames) > *option.Max {
					countText += " :warning:"
				}

				// Option context text
				optionText := fmt.Sprintf("*%s* (%s): %s", option.Name, countText, strings.Join(userNames, ", "))
				optionCtx := slack.NewTextBlockObject("mrkdwn", optionText, false, false)
				contextElements = append(contextElements, optionCtx)
			}
		}

		// Context block for the day
		ctxBlock := slack.NewContextBlock("", contextElements...)

		// Add the constructed blocks for this day
		blocks = append(blocks, slack.NewSectionBlock(sectionTextBlock, nil, slack.NewAccessory(availableOption)), ctxBlock, slack.NewDividerBlock())
	}

	// Build Message with blocks created above
	params := slack.MsgOptionBlocks(blocks...)

	return params
}
