package bot

import "fmt"

type UserID string

type Poll struct {
	Channel           Channel             `json:"channel"`
	OverviewMessageID string              `json:"overview_id"`
	MessageID         string              `json:"message_id"`
	Date              string              `json:"date"`
	Results           map[string][]UserID `json:"results"`
}

func (b *Bot) SetPollResult(poll *Poll, channel Channel, date string, userID UserID, option Option) {
	key := fmt.Sprintf("poll:%s:%s", channel, date)

	if poll.Results == nil {
		poll.Results = make(map[string][]UserID)
	}

	for opt, users := range poll.Results {
		for i, id := range users {
			if id == userID {
				poll.Results[opt] = append(users[:i], users[i+1:]...)
				break
			}
		}
	}

	poll.Results[option.Name] = append(poll.Results[option.Name], userID)

	err := b.Storage.Set(key, poll)
	if err != nil {
		fmt.Println(err)
	}
}

func (b *Bot) GetPoll(channel Channel, date string) *Poll {
	key := fmt.Sprintf("poll:%s:%s", channel, date)

	var poll Poll
	err := b.Storage.Get(key, &poll)
	if err != nil {
		fmt.Println(err)
	}

	return &poll
}

func (b *Bot) SetPoll(channel Channel, date string, poll *Poll) {
	key := fmt.Sprintf("poll:%s:%s", channel, date)

	err := b.Storage.Set(key, poll)
	if err != nil {
		fmt.Println(err)
	}
}
