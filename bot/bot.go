package bot

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/robfig/cron/v3"
	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
	"github.com/slack-go/slack/socketmode"
	"gitlab.com/zegerius/officebot/internal/config"
	"gitlab.com/zegerius/officebot/internal/message"
	"gitlab.com/zegerius/officebot/internal/storage"
)

type Bot struct {
	Config      *config.Config
	Storage     *storage.Storage
	SlackClient *slack.Client
	SlackSocket *socketmode.Client
	Cron        *cron.Cron
	Channels    map[Channel]*ChannelConfig
}

func NewBot(cfg *config.Config, redis *redis.Client) *Bot {
	storage := storage.NewStorage(cfg)

	slackClient := slack.New(
		cfg.SlackBotToken,
		slack.OptionDebug(true),
		slack.OptionLog(log.New(os.Stdout, "api: ", log.Lshortfile|log.LstdFlags)),
		slack.OptionAppLevelToken(cfg.SlackAppToken),
	)

	cron := cron.New()
	cron.Start()

	bot := &Bot{
		Config:      cfg,
		Storage:     storage,
		SlackClient: slackClient,
		Cron:        cron,
		Channels:    make(map[Channel]*ChannelConfig),
	}

	channels := bot.GetChannels()
	fmt.Printf("Found %d channels\n", len(channels))
	for _, channel := range channels {
		bot.GetChannelConfig(channel)
		fmt.Printf("Loaded config for channel %s\n", channel)
		fmt.Printf("Config %+v\n", bot.Channels[channel])
	}

	return bot
}

func (b *Bot) HandleSocketEvent() {
	for evt := range b.SlackSocket.Events {
		switch evt.Type {
		case socketmode.EventTypeConnecting:
			b.handleConnectingEvent()
		case socketmode.EventTypeConnectionError:
			b.handleConnectionErrorEvent()
		case socketmode.EventTypeConnected:
			b.handleConnectedEvent()
		case socketmode.EventTypeEventsAPI:
			b.handleEventsAPI(evt)
		case socketmode.EventTypeInteractive:
			b.handleInteractiveEvent(evt)
		default:
			b.SlackSocket.Debugf("unsupported event type received: %s", evt.Type)
		}
	}
}

func (b *Bot) handleConnectingEvent() {
	fmt.Println("Connecting to Slack with Socket Mode...")
}

func (b *Bot) handleConnectionErrorEvent() {
	fmt.Println("Connection failed. Retrying later...")
}

func (b *Bot) handleConnectedEvent() {
	fmt.Println("Connected to Slack with Socket Mode.")
}

func (b *Bot) handleEventsAPI(evt socketmode.Event) {
	eventsAPIEvent, ok := evt.Data.(slackevents.EventsAPIEvent)
	if !ok {
		b.SlackSocket.Debugf("unsupported Events API event received")
		return
	}

	b.SlackSocket.Ack(*evt.Request)

	switch eventsAPIEvent.Type {
	case slackevents.CallbackEvent:
		b.handleCallbackEvent(evt)
	default:
		b.SlackSocket.Debugf("unsupported Events API callback event received")
	}
}

func (b *Bot) handleCallbackEvent(evt socketmode.Event) {
	innerEvent := evt.Data.(slackevents.EventsAPIEvent).InnerEvent
	switch innerEvent.Data.(type) {
	case *slackevents.AppMentionEvent:
		appMentionEvent := innerEvent.Data.(*slackevents.AppMentionEvent)
		b.handleMentionEvent(&evt, *appMentionEvent)
	}
}

func (b *Bot) handleInteractiveEvent(evt socketmode.Event) {
	callback, ok := evt.Data.(slack.InteractionCallback)
	if !ok {
		fmt.Printf("Ignored %+v\n", evt)
		return
	}

	var payload interface{}
	b.SlackSocket.Ack(*evt.Request, payload)

	switch callback.Type {
	case slack.InteractionTypeBlockActions:
		b.handleInteractionEvent(&evt, callback)
	default:
		b.SlackSocket.Debugf("unsupported Interaction event received")
	}
}

func (b *Bot) handleMentionEvent(event *socketmode.Event, appMentionEvent slackevents.AppMentionEvent) {
	channel := Channel(appMentionEvent.Channel)

	var commandStr string

	commandStr = "register channel:"
	index := strings.Index(string(appMentionEvent.Text), commandStr)
	if index != -1 {
		var config ChannelConfig
		jsonPayload := strings.TrimSpace(appMentionEvent.Text[index+len(commandStr):])
		err := json.Unmarshal([]byte(jsonPayload), &config)
		if err != nil {
			b.SlackSocket.Debugf("Error unmarshalling JSON: %s", err)
			return
		}

		fmt.Printf("Config %+v\n", config)

		// check if channel is already registered
		if _, ok := b.Channels[channel]; ok {
			fmt.Printf("Channel %s already registered", channel)
			// unregister jobs before registering again
			for _, entryId := range b.Channels[channel].CronJobIDs {
				b.UnregisterJob(entryId)
			}
			b.Channels[channel].CronJobIDs = nil
		}

		// update or add channel config
		b.AddChannel(channel, &config)
		fmt.Printf("Registered channel %s\n", channel)

		b.RegisterJobs(channel, &config)
		return
	}

	if strings.Contains(string(appMentionEvent.Text), "config") {
		config := b.Channels[channel]
		configStr := fmt.Sprintf("%v", config)
		b.SlackClient.PostMessage(string(channel), slack.MsgOptionText("", false), slack.MsgOptionText(configStr, false))
		return
	}

	if strings.Contains(string(appMentionEvent.Text), "overview") {
		b.SendOverviewMsg(channel, *b.GetChannelConfig(channel))()
		return
	}

	// register user attributes
	commandStr = "add user attribute:"
	index = strings.Index(string(appMentionEvent.Text), commandStr)
	if index != -1 {
		commandContent := strings.TrimSpace(appMentionEvent.Text[index+len(commandStr):])

		// Split the command content into user mentions and emojis
		parts := strings.Fields(commandContent)
		if len(parts) >= 2 {
			userMention := parts[0]                  // First part should be the user mention
			emojiStr := strings.Join(parts[1:], " ") // Concatenate remaining parts for emojis

			// Extract userID from the user mention
			userID := strings.Trim(userMention, "<@>")

			// Handle different emoji separators: space, comma, or nothing
			emojiList := strings.FieldsFunc(emojiStr, func(r rune) bool {
				return r == ' ' || r == ','
			})

			// Remove empty strings from emojiList, which can happen with multiple separators
			emojiList = filterNonEmptyStrings(emojiList)

			userName := fmt.Sprintf("<@%s>", userID)

			// Overwrite existing emojis
			b.Channels[channel].UserAttrs[userName] = emojiList
			b.AddChannel(channel, b.Channels[channel])
		}
	}
}

// Helper function to filter out empty strings from a slice
func filterNonEmptyStrings(slice []string) []string {
	var result []string
	for _, str := range slice {
		if str != "" {
			result = append(result, str)
		}
	}
	return result
}

func (b *Bot) handleInteractionEvent(event *socketmode.Event, callback slack.InteractionCallback) {
	channel := Channel(callback.Channel.ID)
	ts := callback.Message.Msg.Timestamp

	dateKey := callback.ActionCallback.BlockActions[0].ActionID
	dateKeyParsed, _ := time.Parse("20060102", dateKey)
	dateStr := dateKeyParsed.Format("Monday, January 2")

	// userName := callback.User.Name
	userID := UserID(callback.User.ID)

	poll := b.GetPoll(channel, dateKey)

	var vote Option
	switch ts {
	case poll.OverviewMessageID:
		vote = Option{
			Name: callback.ActionCallback.BlockActions[0].SelectedOption.Value,
		}
	case poll.MessageID:
		vote = Option{
			Name: callback.ActionCallback.BlockActions[0].Value,
		}
	}

	b.SetPollResult(poll, channel, dateKey, userID, vote)

	if poll.MessageID != "" {
		params := generateDayMessage(poll, b.GetChannelConfig(channel), dateStr, dateKey)
		b.SlackClient.UpdateMessage(string(channel), ts, params)
	}

	if poll.OverviewMessageID != "" {
		workWeek := message.GetWorkWeekForDate(dateKeyParsed)

		var polls = make(map[string]*Poll)
		for _, day := range workWeek {
			dateKey := day[1]
			poll := b.GetPoll(channel, dateKey)
			polls[dateKey] = poll
		}

		params := generateWeekMessage(polls, b.GetChannelConfig(channel), workWeek)
		b.SlackClient.UpdateMessage(string(channel), poll.OverviewMessageID, params)
	}
}

func (b *Bot) AddPin(channel Channel, ts string) {
	ref := slack.NewRefToMessage(string(channel), ts)
	b.SlackClient.AddPin(string(channel), ref)
}

func (b *Bot) ClearPins(channel Channel) {
	pins, _, _ := b.SlackClient.ListPins(string(channel)) // Paging not supported for pins, whatever
	for i := 0; i < len(pins); i++ {
		pinMsg := pins[i].Message
		if pinMsg.BotProfile != nil {
			if pinMsg.BotProfile.AppID == b.Config.SlackAppID {
				ref := slack.NewRefToMessage(string(channel), pinMsg.Timestamp)
				b.SlackClient.RemovePin(string(channel), ref)
			}
		}
	}
}

func (b *Bot) Run() {
	sock := socketmode.New(
		b.SlackClient,
		socketmode.OptionDebug(true),
		socketmode.OptionLog(log.New(os.Stdout, "socketmode: ", log.Lshortfile|log.LstdFlags)),
	)

	b.SlackSocket = sock

	go b.HandleSocketEvent()

	for channel, config := range b.Channels {
		b.RegisterJobs(channel, config)
	}

	sock.Run()
}
