package bot

import (
	"fmt"

	"github.com/robfig/cron/v3"
)

func (b *Bot) RegisterJobs(channel Channel, config *ChannelConfig) error {
	// first unregister any existing jobs for this channel
	cronWeeklySchedule := fmt.Sprintf("CRON_TZ=%s %s", config.Timezone, config.CronSchedule.Overview)
	configCopy := *config
	entryIdOverview, err := b.Cron.AddFunc(cronWeeklySchedule, b.SendOverviewMsg(channel, configCopy))
	if err != nil {
		fmt.Println(err)
	}

	cronDailySchedule := fmt.Sprintf("CRON_TZ=%s %s", config.Timezone, config.CronSchedule.Single)
	entryIdSingle, err := b.Cron.AddFunc(cronDailySchedule, b.SendSingleMsg(channel, configCopy))
	if err != nil {
		fmt.Println(err)
	}

	config.CronJobIDs = []cron.EntryID{entryIdOverview, entryIdSingle}
	fmt.Printf("Registered jobs (%v) for channel %s\n", config.CronJobIDs, channel)

	return nil
}

func (b *Bot) UnregisterJob(cronJobID cron.EntryID) {
	b.Cron.Remove(cronJobID)
}
