package config

import (
	"fmt"
	"os"
	"strings"
)

type Config struct {
	SlackAppID    string
	SlackAppToken string
	SlackBotToken string
	RedisURL      string
}

func NewConfig() *Config {
	cfg := &Config{}

	redisURL := os.Getenv("REDIS_URL")
	if redisURL == "" {
		redisURL = "localhost:6379"
	}

	cfg.RedisURL = redisURL

	appToken := os.Getenv("SLACK_APP_TOKEN")
	if appToken == "" {
		fmt.Fprintf(os.Stderr, "SLACK_APP_TOKEN must be set.\n")
		os.Exit(1)
	}

	if !strings.HasPrefix(appToken, "xapp-") {
		fmt.Fprintf(os.Stderr, "SLACK_APP_TOKEN must have the prefix \"xapp-\".")
		os.Exit(1)
	}

	cfg.SlackAppToken = appToken

	botToken := os.Getenv("SLACK_BOT_TOKEN")
	if botToken == "" {
		fmt.Fprintf(os.Stderr, "SLACK_BOT_TOKEN must be set.\n")
		os.Exit(1)
	}

	if !strings.HasPrefix(botToken, "xoxb-") {
		fmt.Fprintf(os.Stderr, "SLACK_BOT_TOKEN must have the prefix \"xoxb-\".")
	}

	cfg.SlackBotToken = botToken

	appID := os.Getenv("SLACK_APP_ID")
	if appToken == "" {
		fmt.Fprintf(os.Stderr, "SLACK_APP_ID must be set.\n")
		os.Exit(1)
	}

	cfg.SlackAppID = appID

	return cfg
}
