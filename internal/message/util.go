package message

import "time"

func GetToday() (dateStr string, dateKey string) {
	now := time.Now()
	return now.Format("Monday, January 2, 2006"), now.Format("2006-01-02")
}

func GetNextWorkDay() (dateStr string, dateKey string) {
	now := time.Now()
	weekday := now.Weekday()

	var delta int
	switch int(weekday) {
	case 5:
		delta = 3
	default:
		delta = 1
	}
	nextDay := now.AddDate(0, 0, delta)
	return nextDay.Format("Monday, January 2"), nextDay.Format("20060102")
}

func GetNextWorkWeek() [][]string {
	now := time.Now()
	daysUntilMonday := int((7 + time.Monday - now.Weekday()) % 7)

	startOfWeek := now.AddDate(0, 0, daysUntilMonday)

	workWeek := make([][]string, 5)
	for i := 0; i < 5; i++ {
		dayOfWeek := startOfWeek.AddDate(0, 0, i)
		workWeek[i] = []string{dayOfWeek.Format("Monday, January 2"), dayOfWeek.Format("20060102")}
	}

	return workWeek
}

func GetWorkWeekForDate(date time.Time) [][]string {
	startOfWeek := date.AddDate(0, 0, -int(date.Weekday())+1)
	workWeek := make([][]string, 5)

	for i := 0; i < 5; i++ {
		dayOfWeek := startOfWeek.AddDate(0, 0, i)
		workWeek[i] = []string{dayOfWeek.Format("Monday, January 2"), dayOfWeek.Format("20060102")}
	}

	return workWeek
}
