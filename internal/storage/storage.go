package storage

import (
	"context"
	"encoding/json"

	"github.com/go-redis/redis/v8"
	"gitlab.com/zegerius/officebot/internal/config"
)

type Storage struct {
	client *redis.Client
}

func NewStorage(cfg *config.Config) *Storage {
	client := redis.NewClient(&redis.Options{
		Addr:     cfg.RedisURL,
		Password: "",
		DB:       0,
	})

	status := client.Ping(context.Background())
	if status.Err() != nil {
		panic(status.Err())
	}

	return &Storage{client: client}
}

func (s *Storage) Set(key string, value interface{}) error {
	ctx := context.Background()
	jsonData, err := json.Marshal(value)
	if err != nil {
		return err
	}
	return s.client.Set(ctx, key, jsonData, 0).Err()
}

func (s *Storage) Get(key string, dest interface{}) error {
	ctx := context.Background()
	val, err := s.client.Get(ctx, key).Result()
	if err != nil {
		return err
	}
	return json.Unmarshal([]byte(val), &dest)
}

func (s *Storage) Delete(key string) error {
	ctx := context.Background()
	return s.client.Del(ctx, key).Err()
}

func (s *Storage) GetKeysByPattern(pattern string) ([]string, error) {
	ctx := context.Background()
	var keys []string
	var cursor uint64
	var err error

	for {
		var ks []string
		ks, cursor, err = s.client.Scan(ctx, cursor, pattern, 10).Result()
		if err != nil {
			return nil, err
		}
		keys = append(keys, ks...)

		if cursor == 0 {
			break
		}
	}

	return keys, nil
}
